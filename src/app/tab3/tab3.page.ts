import { Component } from '@angular/core';
import {AlertController, ToastController} from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(
    private alertController: AlertController,
    private toastController: ToastController,
  ) {

  }

  async creaAlert() {
    const alerta = await this.alertController.create({
      header: 'Pon un nombre',
      message: 'Escribe un nombre y al aceptar aparecerá en la consola',
      inputs: [
        {
          name: 'nombre',
          type: 'text',
          placeholder: 'Escribe tu nombre'
        }
      ],
      buttons: [
        { text: 'Aceptar',
          handler: (alertData) => {
            console.log('Se ha escrito', alertData.nombre);
          }
        },
        { text: 'Cancelar' }
      ]
    });
    await alerta.present();
  }

  async selectColor(event) {
    const toast = await this.toastController.create({
      message: 'Has elegido ' + event.target.value,
      duration: 2000,
    });
    await toast.present();
  }
}
