import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  item
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    const  navigation = this.router.getCurrentNavigation();
    const {state} = navigation.extras;
    state && (this.item = state.item);
  }

}
