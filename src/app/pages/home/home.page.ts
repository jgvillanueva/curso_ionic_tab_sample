import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {Content} from '@angular/compiler/src/render3/r3_ast';
import {IonContent, IonInfiniteScroll, LoadingController} from '@ionic/angular';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  data = [];
  filteredData
  @ViewChild('pageContent') pageContent: IonContent;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  filter = true;

  constructor(
    private apiService: ApiServiceService,
    private router: Router,
    private loadingController: LoadingController,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.refresh();
  }

  ionViewDidLeave() {
    this.clearData();
  }

  clearData() {
    this.data = [];
  }

  async refresh() {
    const loader = await this.loadingController.create({
      message: 'Cargando datos...',
      translucent: true,
    });
    await loader.present();
    this.apiService.getMessages()
      .subscribe(async (response) => {
        this.data = this.data.concat(response);
        this.filterItems(null);
        this.infiniteScroll.complete();
        //this.pageContent.scrollToTop();
        await loader.dismiss();
      });
  }

  deleteMessage(id) {
    this.apiService.deleteService(id)
      .subscribe((response) => {
        this.refresh();
      });
  }

  goDetail(item) {
    const navigationExtras: NavigationExtras = {
      state: {
        item,
      }
    }
    this.router.navigate(['detail'], navigationExtras);
  }

  filterItems(event) {
    if (event) {
      const value = event.target.value.toLowerCase();
      this.filteredData = this.data.filter((item) => {
        return item.asunto.toLowerCase().indexOf(value) !== -1;
      });
    } else {
      this.filteredData = this.data;
    }
  }

  async loadMoreData() {
    await this.refresh();
  }

}
