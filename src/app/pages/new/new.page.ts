import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiServiceService} from '../../services/api-service.service';
import {Router} from '@angular/router';
import {ActionSheetController, AlertController} from '@ionic/angular';

@Component({
  selector: 'app-new',
  templateUrl: './new.page.html',
  styleUrls: ['./new.page.scss'],
})
export class NewPage implements OnInit {
  mensaje: FormGroup;
  primary = true;


  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiServiceService,
    private router: Router,
    private alertController: AlertController,
    private actionSheetController: ActionSheetController,
  ) {
    this.mensaje = this.formBuilder.group({
      asunto: ['', [Validators.required, Validators.minLength(10)]],
      mensaje: ['', [Validators.required, Validators.minLength(10)]],
      user_id: ['13'],
    });
  }

  ngOnInit() {
  }

  enviaForm() {
    this.apiService.postMessage(this.mensaje.value)
      .subscribe(
        async (response) => {
          await this.showAlert('Info', "mensaje enviado correctamente");
          this.router.navigate(['tabs/home']);
        },
        async (error) => {
          console.log('Error al enviar los datos', error.message);
          await this.showAlert('Info', 'Error al enviar los datos' + error.message);
        }
      );
  }

  async showAlert(title, message) {
    return new Promise(async (resolve) => {
        const alerta = await this.alertController.create({
          header: title,
          message: message,
          buttons: [
            {
              text: 'ok',
              handler: () => {
                resolve();
              }
            },
          ]
        });
        await alerta.present();
      }
    );
  }

  async showListado() {
    const actionSheet = await this.actionSheetController.create({
      header: 'usuarios',
      buttons: [
        {
          text: 'Jorge',
          handler: async () => {
            this.mensaje.controls.user_id.setValue('13');
          }
        },
        {
          text: 'David',
          handler: async () => {
            await this.mensaje.controls.user_id.setValue('14');
          }
        },
        {
          text: 'Cristina',
          handler: async () => {
            await this.mensaje.controls.user_id.setValue('15');
          }
        }
      ]
    });
    await actionSheet.present();
  }

}
