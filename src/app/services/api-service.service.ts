import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  API_URL = 'http://dev.contanimacion.com/api_tablon/api/';
  data

  constructor(
    private httpClient: HttpClient
  ) { }

  getMessages() {
    return this.httpClient.get(this.API_URL + 'mensajes');
  }

  deleteService(id) {
    return this.httpClient.get(this.API_URL + 'mensajes/delete/' + id);
  }

  postMessage(item) {
    return this.httpClient.post(this.API_URL + 'mensajes/add', item);
  }
}
